<?php
    /**
     * Hash Class
     *
     * String encryption and decryption with Argon2 of PHP7.2
     *
     * @author Arvin Kent S. Lazaga
     * @since May 31,2019
     * @copyright 2019
     */
    class Hash {
        private $str;
        private $hashed;

        /**
         * Encrypts strings
         *
         * @param string $str
         * @return string $str
         * @access public
         */
        public function encrypt($str) {
            $this->str = password_hash($str, PASSWORD_ARGON2I);

            return $this->str;
        }

        /**
         * Decrypts hashed strings
         * 
         * @param string str
         * @return boolean
         * @access public
         */
        public function decrypt($str, $hashed) {
            if (password_verify($str, $hashed)) {
                return true;
            }

            return false;
        }
    }

    /** 
     * Sample usage for encrypting and decrypting strings with Argon2 
     */
    $hash = new Hash();
    $str = 'Arvin Kent S. Lazaga';
    $hashedStr = $hash->encrypt($str);
    
    echo 'Encrypted: ' . $hashedStr . PHP_EOL;
    echo 'String: ' . $str . PHP_EOL;
    echo 'Matched: ' . ($hash->decrypt($str, $hashedStr) == 1 ? 'true' : 'false');    